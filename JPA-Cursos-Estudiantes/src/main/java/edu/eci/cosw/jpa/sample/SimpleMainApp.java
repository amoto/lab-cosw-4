/*
 * Copyright (C) 2016 hcadavid
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package edu.eci.cosw.jpa.sample;

import edu.eci.cosw.jpa.sample.model.Curso;
import edu.eci.cosw.jpa.sample.model.Estudiante;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author hcadavid
 */
public class SimpleMainApp {
   
    public static void main(String a[]){
        SessionFactory sf=getSessionFactory();
        Session s=sf.openSession();
        Transaction tx=s.beginTransaction();
        Estudiante e= new Estudiante(2105684,"Julian Devia");
        Estudiante e2= new Estudiante(2107646,"Daniela Sepulveda");
        Set<Estudiante> se= new HashSet<>();
        se.add(e);se.add(e2);
        Curso c= new Curso(0654,"Teoria de la computacion","TCOM",se);
        Curso c2= new Curso(0456,"Estadistica","ESTI",se);
        Set<Curso> sc=new HashSet<>();
        sc.add(c);sc.add(c2);
        e.setCursos(sc);
        e2.setCursos(sc);
        s.saveOrUpdate(c);
        s.saveOrUpdate(c2);
        s.saveOrUpdate(e);
        s.saveOrUpdate(e2);
        System.out.println("----------------------------->"+((Estudiante)s.load(Estudiante.class,new Integer(2107646))).getNombre());
        System.out.println(((Estudiante)s.load(Estudiante.class,new Integer(2105684))).getNombre());
        System.out.println(((Curso)s.load(Curso.class,new Integer(0456))).getNombre());
        System.out.println(((Curso)s.load(Curso.class,new Integer(0654))).getNombre());
        tx.commit(); 
        s.close();
        sf.close();
    }

    public static SessionFactory getSessionFactory() {
        // loads configuration and mappings
        Configuration configuration = new Configuration().configure("hibernate.cfg.xml");
        ServiceRegistry serviceRegistry
                = new StandardServiceRegistryBuilder()
                .applySettings(configuration.getProperties()).build();

        // builds a session factory from the service registry
        SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        return sessionFactory;
    }

}
